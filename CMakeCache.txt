# This is the CMakeCache file.
# For build in directory: /home/atlas/yonsi-example
# It was generated by CMake: /usr/local/bin/cmake
# You can edit this file to change values found and used by cmake.
# If you do not want to change any of the values, simply exit the editor.
# If you do want to change a value, simply edit, save, and exit the editor.
# The syntax for the file is as follows:
# KEY:TYPE=VALUE
# KEY is the name of a variable in the cache.
# TYPE is a hint to GUIs for the type of VALUE, DO NOT EDIT TYPE!.
# VALUE is the current value for the KEY.

########################
# EXTERNAL cache entries
########################

//Default optimisation settings for Debug mode
ATLAS_CXX_FLAGS_DEBUG:STRING=-g

//Default optimisation settings for Release mode
ATLAS_CXX_FLAGS_RELEASE:STRING=-DNDEBUG -O2

//Default optimisation settings for RelWithDebInfo mode
ATLAS_CXX_FLAGS_RELWITHDEBINFO:STRING=-DNDEBUG -O2 -g

//Checker(s) to activate during compilation
ATLAS_GCC_CHECKERS:STRING=

//Enable using the GCC checker plugins if they are available
ATLAS_USE_GCC_CHECKERS:BOOL=ON

//The directory containing a CMake configuration file for AnalysisBaseExternals.
AnalysisBaseExternals_DIR:PATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/cmake

//The directory containing a CMake configuration file for AnalysisBase.
AnalysisBase_DIR:PATH=/usr/AnalysisBase/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/cmake

//Value Computed by CMake
AnalysisPayload_BINARY_DIR:STATIC=/home/atlas/yonsi-example

//Value Computed by CMake
AnalysisPayload_SOURCE_DIR:STATIC=/home/atlas/yonsi-example/lbnl-bootcamp

//Path to a program.
CMAKE_AR:FILEPATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin/ar

//Build mode for the release
CMAKE_BUILD_TYPE:STRING=RelWithDebInfo

//Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

//CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/opt/lcg/gcc/6.2.0binutils/x86_64-slc6/bin/g++

//A wrapper around 'ar' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_CXX_COMPILER_AR:FILEPATH=/opt/lcg/gcc/6.2.0binutils/x86_64-slc6/bin/gcc-ar

//A wrapper around 'ranlib' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_CXX_COMPILER_RANLIB:FILEPATH=/opt/lcg/gcc/6.2.0binutils/x86_64-slc6/bin/gcc-ranlib

//(Dis)allow using GNU extensions
CMAKE_CXX_EXTENSIONS:BOOL=FALSE

//Flags used by the CXX compiler during all build types.
CMAKE_CXX_FLAGS:STRING=

//Compiler setting
CMAKE_CXX_FLAGS_DEBUG:STRING=-g -Wall -Wno-long-long -Wno-deprecated -Wno-unused-local-typedefs -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wextra -Werror=return-type -fsanitize=undefined -pedantic

//Flags used by the CXX compiler during MINSIZEREL builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Compiler setting
CMAKE_CXX_FLAGS_RELEASE:STRING=-DNDEBUG -O2 -Wall -Wno-long-long -Wno-deprecated -Wno-unused-local-typedefs -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wextra -Werror=return-type -pedantic

//Compiler setting
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-DNDEBUG -O2 -g -Wall -Wno-long-long -Wno-deprecated -Wno-unused-local-typedefs -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wextra -Werror=return-type -pedantic

//C++ standard used for the build
CMAKE_CXX_STANDARD:STRING=14

//C compiler
CMAKE_C_COMPILER:FILEPATH=/opt/lcg/gcc/6.2.0binutils/x86_64-slc6/bin/gcc

//A wrapper around 'ar' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_C_COMPILER_AR:FILEPATH=/opt/lcg/gcc/6.2.0binutils/x86_64-slc6/bin/gcc-ar

//A wrapper around 'ranlib' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_C_COMPILER_RANLIB:FILEPATH=/opt/lcg/gcc/6.2.0binutils/x86_64-slc6/bin/gcc-ranlib

//Flags used by the C compiler during all build types.
CMAKE_C_FLAGS:STRING=

//Flags used by the C compiler during DEBUG builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g

//Flags used by the C compiler during MINSIZEREL builds.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the C compiler during RELEASE builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the C compiler during RELWITHDEBINFO builds.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//Compiler setting
CMAKE_EXE_LINKER_FLAGS:STRING= -Wl,--as-needed -Wl,--hash-style=both

//Flags used by the linker during DEBUG builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during MINSIZEREL builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during RELEASE builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during RELWITHDEBINFO builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=OFF

//Framework finding behaviour on macOS
CMAKE_FIND_FRAMEWORK:STRING=LAST

//Compiler setting
CMAKE_Fortran_FLAGS:STRING= -ffixed-line-length-132 -DFVOIDP=INTEGER*8

//Compiler setting
CMAKE_Fortran_FLAGS_RELEASE:STRING= -funroll-all-loops

//Compiler setting
CMAKE_Fortran_FLAGS_RELWITHDEBINFO:STRING= -funroll-all-loops

//Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr/local

//Path to a program.
CMAKE_LINKER:FILEPATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin/ld

//Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/bin/gmake

//Compiler setting
CMAKE_MODULE_LINKER_FLAGS:STRING= -Wl,--as-needed -Wl,--no-undefined -Wl,-z,max-page-size=0x1000 -Wl,--hash-style=both

//Flags used by the linker during the creation of modules during
// DEBUG builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of modules during
// MINSIZEREL builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of modules during
// RELEASE builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of modules during
// RELWITHDEBINFO builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_NM:FILEPATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin/nm

//Path to a program.
CMAKE_OBJCOPY:FILEPATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin/objcopy

//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin/objdump

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=AnalysisPayload

//Path to a program.
CMAKE_RANLIB:FILEPATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin/ranlib

//Compiler setting
CMAKE_SHARED_LINKER_FLAGS:STRING= -Wl,--as-needed -Wl,--no-undefined -Wl,-z,max-page-size=0x1000 -Wl,--hash-style=both

//Flags used by the linker during the creation of shared libraries
// during DEBUG builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of shared libraries
// during MINSIZEREL builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of shared libraries
// during RELEASE builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of shared libraries
// during RELWITHDEBINFO builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//If set, runtime paths are not added when installing shared libraries,
// but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

//If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

//Flags used by the linker during the creation of static libraries
// during all build types.
CMAKE_STATIC_LINKER_FLAGS:STRING=

//Flags used by the linker during the creation of static libraries
// during DEBUG builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of static libraries
// during MINSIZEREL builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of static libraries
// during RELEASE builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of static libraries
// during RELWITHDEBINFO builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_STRIP:FILEPATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin/strip

//If this value is on, makefiles will be generated without the
// .SILENT directive, and all commands will be echoed to the console
// during the make.  This is useful for debugging only. With Visual
// Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE

//LCG compiler version
LCG_COMPVERS:STRING=49

//The directory containing a CMake configuration file for LCG.
LCG_DIR:PATH=/usr/AnalysisBase/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/cmake

//Directory holding LCG releases
LCG_RELEASE_BASE:PATH=/cvmfs/sft.cern.ch/lcg/releases

//LCG version used
LCG_VERSION:STRING=0

//LCG version (just the number) used
LCG_VERSION_NUMBER:STRING=0

//LCG version postfix used
LCG_VERSION_POSTFIX:STRING=

//Directory holding LCG releases
LCG_releases_base:PATH=/cvmfs/sft.cern.ch/lcg/releases

//Flag specifying that this is a "RootCore" release
ROOTCORE:BOOL=TRUE

//Path to a library.
ROOT_Core_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libCore.so

//The directory containing a CMake configuration file for ROOT.
ROOT_DIR:PATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/cmake

//Path to a library.
ROOT_Gpad_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libGpad.so

//Path to a library.
ROOT_Graf3d_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libGraf3d.so

//Path to a library.
ROOT_Graf_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libGraf.so

//Path to a library.
ROOT_Hist_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libHist.so

//Path to a library.
ROOT_Imt_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libImt.so

//Path to a library.
ROOT_MathCore_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libMathCore.so

//Path to a library.
ROOT_Matrix_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libMatrix.so

//Path to a library.
ROOT_MultiProc_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libMultiProc.so

//Path to a library.
ROOT_Net_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libNet.so

//Path to a library.
ROOT_Physics_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libPhysics.so

//Path to a library.
ROOT_Postscript_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libPostscript.so

//Path to a library.
ROOT_RIO_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libRIO.so

//Path to a library.
ROOT_ROOTDataFrame_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libROOTDataFrame.so

//Path to a library.
ROOT_Rint_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libRint.so

//Path to a library.
ROOT_Thread_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libThread.so

//Path to a library.
ROOT_TreePlayer_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libTreePlayer.so

//Path to a library.
ROOT_Tree_LIBRARY:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/lib/libTree.so

//Path to a program.
ROOT_genmap_CMD:FILEPATH=ROOT_genmap_CMD-NOTFOUND

//Path to a program.
ROOT_genreflex_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/genreflex

//Path to a program.
ROOT_hadd_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/hadd

//Path to a program.
ROOT_root_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/root

//Path to a program.
ROOT_rootbrowse_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootbrowse

//Path to a program.
ROOT_rootcint_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootcint

//Path to a program.
ROOT_rootcling_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootcling

//Path to a program.
ROOT_rootcp_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootcp

//Path to a program.
ROOT_rootdraw_CMD:FILEPATH=ROOT_rootdraw_CMD-NOTFOUND

//Path to a program.
ROOT_rootls_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootls

//Path to a program.
ROOT_rootmkdir_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootmkdir

//Path to a program.
ROOT_rootmv_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootmv

//Path to a program.
ROOT_rootrm_CMD:FILEPATH=/usr/AnalysisBaseExternals/21.2.75/InstallArea/x86_64-slc6-gcc62-opt/bin/rootrm

//Prefer using the -pthread compiler flag over -lpthread
THREADS_PREFER_PTHREAD_FLAG:BOOL=TRUE

//Flag specifying that this is an analysis release
XAOD_ANALYSIS:BOOL=TRUE

//Flag specifying that this is a standalone build
XAOD_STANDALONE:BOOL=TRUE

//Path to a library.
_gccchecker:FILEPATH=_gccchecker-NOTFOUND


########################
# INTERNAL cache entries
########################

//ADVANCED property for variable: CMAKE_AR
CMAKE_AR-ADVANCED:INTERNAL=1
//This is the directory where this CMakeCache.txt was created
CMAKE_CACHEFILE_DIR:INTERNAL=/home/atlas/yonsi-example
//Major version of cmake used to create the current loaded cache
CMAKE_CACHE_MAJOR_VERSION:INTERNAL=3
//Minor version of cmake used to create the current loaded cache
CMAKE_CACHE_MINOR_VERSION:INTERNAL=11
//Patch version of cmake used to create the current loaded cache
CMAKE_CACHE_PATCH_VERSION:INTERNAL=4
//ADVANCED property for variable: CMAKE_COLOR_MAKEFILE
CMAKE_COLOR_MAKEFILE-ADVANCED:INTERNAL=1
//Path to CMake executable.
CMAKE_COMMAND:INTERNAL=/usr/local/bin/cmake
//Path to cpack program executable.
CMAKE_CPACK_COMMAND:INTERNAL=/usr/local/bin/cpack
//Path to ctest program executable.
CMAKE_CTEST_COMMAND:INTERNAL=/usr/local/bin/ctest
//ADVANCED property for variable: CMAKE_CXX_COMPILER
CMAKE_CXX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_COMPILER_AR
CMAKE_CXX_COMPILER_AR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_COMPILER_RANLIB
CMAKE_CXX_COMPILER_RANLIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS
CMAKE_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_DEBUG
CMAKE_CXX_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_MINSIZEREL
CMAKE_CXX_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELEASE
CMAKE_CXX_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELWITHDEBINFO
CMAKE_CXX_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER
CMAKE_C_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER_AR
CMAKE_C_COMPILER_AR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER_RANLIB
CMAKE_C_COMPILER_RANLIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS
CMAKE_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_DEBUG
CMAKE_C_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_MINSIZEREL
CMAKE_C_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELEASE
CMAKE_C_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELWITHDEBINFO
CMAKE_C_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Path to cache edit program executable.
CMAKE_EDIT_COMMAND:INTERNAL=/usr/local/bin/ccmake
//Executable file format
CMAKE_EXECUTABLE_FORMAT:INTERNAL=ELF
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS
CMAKE_EXE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_DEBUG
CMAKE_EXE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELEASE
CMAKE_EXE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_EXPORT_COMPILE_COMMANDS-ADVANCED:INTERNAL=1
//Name of external makefile project generator.
CMAKE_EXTRA_GENERATOR:INTERNAL=
//Name of generator.
CMAKE_GENERATOR:INTERNAL=Unix Makefiles
//Generator instance identifier.
CMAKE_GENERATOR_INSTANCE:INTERNAL=
//Name of generator platform.
CMAKE_GENERATOR_PLATFORM:INTERNAL=
//Name of generator toolset.
CMAKE_GENERATOR_TOOLSET:INTERNAL=
//Have symbol pthread_create
CMAKE_HAVE_LIBC_CREATE:INTERNAL=
//Have include pthread.h
CMAKE_HAVE_PTHREAD_H:INTERNAL=1
//Source directory with the top level CMakeLists.txt file for this
// project
CMAKE_HOME_DIRECTORY:INTERNAL=/home/atlas/yonsi-example/lbnl-bootcamp
//Install .so files without execute permission.
CMAKE_INSTALL_SO_NO_EXE:INTERNAL=0
//ADVANCED property for variable: CMAKE_LINKER
CMAKE_LINKER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MAKE_PROGRAM
CMAKE_MAKE_PROGRAM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS
CMAKE_MODULE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_DEBUG
CMAKE_MODULE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELEASE
CMAKE_MODULE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_NM
CMAKE_NM-ADVANCED:INTERNAL=1
//number of local generators
CMAKE_NUMBER_OF_MAKEFILES:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJCOPY
CMAKE_OBJCOPY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJDUMP
CMAKE_OBJDUMP-ADVANCED:INTERNAL=1
//Platform information initialized
CMAKE_PLATFORM_INFO_INITIALIZED:INTERNAL=1
//ADVANCED property for variable: CMAKE_RANLIB
CMAKE_RANLIB-ADVANCED:INTERNAL=1
//Path to CMake installation.
CMAKE_ROOT:INTERNAL=/usr/local/share/cmake-3.11
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS
CMAKE_SHARED_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_DEBUG
CMAKE_SHARED_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELEASE
CMAKE_SHARED_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_INSTALL_RPATH
CMAKE_SKIP_INSTALL_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_RPATH
CMAKE_SKIP_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS
CMAKE_STATIC_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_DEBUG
CMAKE_STATIC_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELEASE
CMAKE_STATIC_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STRIP
CMAKE_STRIP-ADVANCED:INTERNAL=1
//uname command
CMAKE_UNAME:INTERNAL=/bin/uname
//ADVANCED property for variable: CMAKE_VERBOSE_MAKEFILE
CMAKE_VERBOSE_MAKEFILE-ADVANCED:INTERNAL=1
//Details about finding Threads
FIND_PACKAGE_MESSAGE_DETAILS_Threads:INTERNAL=[TRUE][v()]
//ADVANCED property for variable: LCG_VERSION
LCG_VERSION-ADVANCED:INTERNAL=1
//ADVANCED property for variable: LCG_VERSION_NUMBER
LCG_VERSION_NUMBER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: LCG_VERSION_POSTFIX
LCG_VERSION_POSTFIX-ADVANCED:INTERNAL=1
//ADVANCED property for variable: LCG_releases_base
LCG_releases_base-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Core_LIBRARY
ROOT_Core_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Gpad_LIBRARY
ROOT_Gpad_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Graf3d_LIBRARY
ROOT_Graf3d_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Graf_LIBRARY
ROOT_Graf_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Hist_LIBRARY
ROOT_Hist_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Imt_LIBRARY
ROOT_Imt_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_MathCore_LIBRARY
ROOT_MathCore_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Matrix_LIBRARY
ROOT_Matrix_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_MultiProc_LIBRARY
ROOT_MultiProc_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Net_LIBRARY
ROOT_Net_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Physics_LIBRARY
ROOT_Physics_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Postscript_LIBRARY
ROOT_Postscript_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_RIO_LIBRARY
ROOT_RIO_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_ROOTDataFrame_LIBRARY
ROOT_ROOTDataFrame_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Rint_LIBRARY
ROOT_Rint_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Thread_LIBRARY
ROOT_Thread_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_TreePlayer_LIBRARY
ROOT_TreePlayer_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_Tree_LIBRARY
ROOT_Tree_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_genreflex_CMD
ROOT_genreflex_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_hadd_CMD
ROOT_hadd_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_root_CMD
ROOT_root_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootbrowse_CMD
ROOT_rootbrowse_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootcint_CMD
ROOT_rootcint_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootcling_CMD
ROOT_rootcling_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootcp_CMD
ROOT_rootcp_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootls_CMD
ROOT_rootls_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootmkdir_CMD
ROOT_rootmkdir_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootmv_CMD
ROOT_rootmv_CMD-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_rootrm_CMD
ROOT_rootrm_CMD-ADVANCED:INTERNAL=1
//Result of TRY_COMPILE
THREADS_HAVE_PTHREAD_ARG:INTERNAL=TRUE

